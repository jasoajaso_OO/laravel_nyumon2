<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\HelloMiddleware;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//上に記述されたものが優先される。
//Controllerを仕様するときはController, webのルート, indexの3か所を編集する

//{id?}は、コントローラー側にも移して使用できる
//※変数を渡すときはfunction がコントローラー側に必要

//post送信はget をpostへ。
//使用するコントローラー追加忘れない
Route::get('hello', 'App\Http\Controllers\HelloController@index')
    ->middleware(HelloMiddleware::class);
Route::post('hello', 'App\Http\Controllers\HelloController@post');

Route::get('/', function () {
    return view('welcome');
});












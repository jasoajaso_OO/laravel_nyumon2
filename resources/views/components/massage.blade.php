<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    body{ font-size:16px; color:#999; }
    h1{font-size:100pt; text-align:right; color:#f6f6f6; margin:-50px; 0px -100px 0;}
    .message{font-size: 20px; background-color: #f00;}
    .msg_title{color:#0f0;}
    .msg_content{color:#00f;}
</style>
<body>
    <div class="message">
        <p class="msg_title">{{$msg_title}}</p>
        <p class="msg_content">{{$msg_content}}</p>
    </div>
    
</body>
</html>
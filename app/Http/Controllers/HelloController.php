<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\HelloRequest;
use Validator;

//{~~?}でアドレス変数 -> indexで{{id?}}で使用
//$msg は $data = ['msg'=>'文章']; return view('hello.index', $data) のようにリターン部分に[''=>''];がくる
//return view('テンプレート(送り先)', $data(配列),)  ※コントローラーのreturn はviewのテンプレートに返すときはviewつける

//引数の使用はコントローラー側

//使う配列は'htmlの名前'で$配列が呼び出されるようにセットする
class HelloController extends Controller
{
    public function index(Request $request)
    {        
        return view('hello.index');
    }
//ここの$request->msg のmsgは、indexのname=msg。
    public function post(Request $request){
        return view('hello.index', ['msg'=>$request->msg]); 
    }
}

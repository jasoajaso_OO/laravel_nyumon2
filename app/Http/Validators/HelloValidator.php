<?php
    namespace App\Http\Validators;
    use Illuminate\Validation\Validator;
    class HelloValidator extends Validator{
        public function validateHello($attribute, $value, $parameters)
        {
            return $value % 2 == 0;
        }
        public function boot(){
            Validator::extend('hello', function($attribute, $value, $parameters, $validator){
                return $value % 2 == 0;
            });
        }
    }

?>
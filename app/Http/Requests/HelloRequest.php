<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class HelloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        if($this->path() == 'hello')
        {
            return true;
        }else{
            return false;
        }
    }
    public function rules()
    {
        return [
            'name'=>'required',
            'mail'=>'email',
            'age'=>'numeric|hello',
        ];
    }
    public function messeage()
    {
        return[
            'name.required'=>'未入力',
            'mail.email'=>'notメール形式',
            'age.numeric'=>'not整数',    
            'age.hello'=>'偶数のみの受けつけ',  
        ];

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

}
